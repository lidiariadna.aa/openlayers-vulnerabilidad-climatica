import './style.css';
import {Map, View} from 'ol';
import KML from 'ol/format/KML.js';
import OSM from 'ol/source/OSM';
import VectorSource from 'ol/source/Vector.js';
import {Circle as CicleStyle, Fill, Stroke, Style} from 'ol/style.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';

const vector = new VectorLayer({
  source: new VectorSource({
    url: 'residuos_urbanos.kml',
    format: new KML({
      extractStyles: false,
    }),
  }),
  style: {
    'circle-stroke-color': 'hsl(0 100% 100% / 0.4)',
    'circle-stroke-width': 0.2,
    'circle-radius': [
      'interpolate',
      ['linear'],
      ['get', 'name'],
      500_000,
      3,
      10_000_000,
      5,
    ],
    'circle-fill-color': [
      'interpolate',
      ['linear'],
      ['get', 'name'],
      1_000_000,
      'hsl(210 100% 40% / 0.5)',
      10_000_000,
      'hsl(0 80% 60% / 0.3)',
    ],
  },
});
  
const raster = new TileLayer({
  source: new OSM(),
});
 
const map = new Map({
  target: 'map',
  layers: [raster, vector],
  view: new View({
    center: [-101, 22],
    projection: "EPSG:4326",
    zoom: 5
  })
});

const info = document.getElementById('info');

let currentFeature;
const displayFeatureInfo = function (pixel, target) {
  const feature = target.closest('.ol-control')
    ? undefined
    : map.forEachFeatureAtPixel(pixel, function (feature) {
        return feature;
      });
  if (feature) {
    info.style.left = pixel[0] + 'px';
    info.style.top = pixel[1] + 'px';
    if (feature !== currentFeature) {
      info.style.visibility = 'visible';
      info.innerText = feature.get('name');
    }
  } else {
    info.style.visibility = 'hidden';
  }
  currentFeature = feature;
};

map.on('pointermove', function (evt) {
  if (evt.dragging) {
    info.style.visibility = 'hidden';
    currentFeature = undefined;
    return;
  }
  const pixel = map.getEventPixel(evt.originalEvent);
  displayFeatureInfo(pixel, evt.originalEvent.target);
});

map.on('click', function (evt) {
  displayFeatureInfo(evt.pixel, evt.originalEvent.target);
});

map.getTargetElement().addEventListener('pointerleave', function () {
  currentFeature = undefined;
  info.style.visibility = 'hidden';
});

